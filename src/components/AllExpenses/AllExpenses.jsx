import './AllExpenses.css';
export default function AllExpenses(){
    return (
    <table class="H_Tablecontainer">
		<thead>
		<tr className='H_tableHead'>
			<th><h2>ID</h2></th>
			<th><h2>Title</h2></th>
			<th><h2>Description</h2></th>
			<th><h2>Amount</h2></th>
			<th><h2>Currency</h2></th>
			<th><h2>Date</h2></th>
			<th><h2>Time</h2></th>
			<th><h2>Category</h2></th>
			<th id='option'><h2>Option</h2></th>
		</tr>
		</thead>

		<tbody>
		<tr className='H_tableRow'>
			<td>1</td>
			<td>title</td>
			<td>description</td>
			<td>amount</td>
			<td>currency</td>
			<td>date</td>
			<td>time</td>
			<td>category</td>
			<td  colSpan='1'>
				<div >
					<button type='button'>edit</button>
					<button type='button'>Delete</button>
				</div>
			</td>

		</tr>
		<tr className='H_tableRow'>
			<td>1</td>
			<td>title</td>
			<td>description</td>
			<td>amount</td>
			<td>currency</td>
			<td>date</td>
			<td>time</td>
			<td>category</td>
			<td  colSpan='1'>
				<div >
					<button type='button'>edit</button>
					<button type='button'>Delete</button>
				</div>
			</td>

		</tr>
		<tr className='H_tableRow'>
			<td>1</td>
			<td>title</td>
			<td>description</td>
			<td>amount</td>
			<td>currency</td>
			<td>date</td>
			<td>time</td>
			<td>category</td>
			<td  colSpan='1'>
				<div >
					<button type='button'>edit</button>
					<button type='button'>Delete</button>
				</div>
			</td>

		</tr>
		<tr className='H_tableRow'>
			<td>1</td>
			<td>title</td>
			<td>description</td>
			<td>amount</td>
			<td>currency</td>
			<td>date</td>
			<td>time</td>
			<td>category</td>
			<td  colSpan='1'>
				<div >
					<button type='button'>edit</button>
					<button type='button'>Delete</button>
				</div>
			</td>

		</tr>
		<tr className='H_tableRow'>
			<td>1</td>
			<td>title</td>
			<td>description</td>
			<td>amount</td>
			<td>currency</td>
			<td>date</td>
			<td>time</td>
			<td>category</td>
			<td  colSpan='1'>
				<div >
					<button type='button'>edit</button>
					<button type='button'>Delete</button>
				</div>
			</td>

		</tr>
		<tr className='H_tableRow'>
			<td>1</td>
			<td>title</td>
			<td>description</td>
			<td>amount</td>
			<td>currency</td>
			<td>date</td>
			<td>time</td>
			<td>category</td>
			<td  colSpan='1'>
				<div >
					<button type='button'>edit</button>
					<button type='button'>Delete</button>
				</div>
			</td>

		</tr>
		</tbody>
</table>
    );
}