import './loginComponent.css';
import { Link, Redirect,useHistory } from "react-router-dom";
export default function loginComponent(){

    return(
		<div class="H_wrapper">
      <div class="H_title">Login Form</div>
      <form action="#">
        <div class="H_field">
          <input type="text" id='username'  required/>
          <label>User name</label>
        </div>
        <div class="H_field">
          <input type="password" id="password"   required/>
          <label>Password</label>
        </div>
        <div class="H_content">
          <div class="H_checkbox">
            <input type="checkbox" id="remember-me"/>
            <label for="remember-me">Remember me</label>
          </div>
          <div class="H_pass-link"><a href="#">Forgot password?</a></div>
        </div>
        <div class="H_field">
      
          <Link to="/Home"><input type="submit" value="Login"/></Link>
        </div>
        <div class="H_signup-link">Not me <a href="#">Signup now</a></div>
      </form>
    </div>
  
  
    );
}