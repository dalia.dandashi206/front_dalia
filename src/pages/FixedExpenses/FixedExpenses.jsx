import { Link } from "react-router-dom";
import "./FixedExpenses.css";
import AddExpense from "../../components/AddExpense/AddExpense";
import AllExpenses from "../../components/AllExpenses/AllExpenses";
import React,{ useEffect,useState } from "react";
import axios from "axios";


export default function FixedExpenses(){


    return (
        <div className="H_FixedExpensesContainer">
            <div className="H_AddExpenseContainer">
            <AddExpense/>
            </div>
            <div className='H_AllExpensesContainer'>
            <AllExpenses/>
            </div>
        </div>
    );
}