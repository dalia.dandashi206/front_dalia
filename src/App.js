import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

//Pages
import Login from './pages/Login/login';
import Home from './pages/home_page/home_page';
import Income from './pages/Income/Income';
import Expence from './pages/Expence/Expence';
import Admin from './pages/Admins/Admins';
import FixedExpenses from './pages/FixedExpenses/FixedExpenses';
import FixedIncomes from './pages/FixedIncomes/FixedIncomes';
import ProfitGoals from './pages/ProfitGoals/ProfitGoals';
import RecuringExpenses from './pages/RecuringExpenses/RcuringExpenses';
import RecuringIncomes from './pages/RecuringIncomes/RecuringIncomes';
import Reports from './pages/Reports/Reports';
import Category from './pages/Category/Category';
// import addcategory from './components/Category/AddCategory/AddCategory';


import './App.css';
function App() {
  return (
    <div className="App">
      
<Router> 
          <Link to="/login"></Link>
          <Link to="/Home"></Link>
          <Link to="/FixedExpenses"></Link>
          <Link to="/FixedIncomes"></Link>
          <Link to="/ProfitGoals"></Link>
          <Link to="/RecuringExpenses"></Link>
          <Link to="/RecuringIncomes"></Link>
          <Link to="/reports"></Link>
          <Link to="/Admin"></Link>
          <Link to="/Income"></Link>
          <Link to="/Expence"></Link>
          <Link to="/Category"></Link>
          
      <Switch>
        <Route exact path='/login' component={Login} />
        <Route exact path='/Home' component={Home} />
        <Route exact path='/FixedExpenses' component={FixedExpenses} />
        <Route exact path='/FixedIncomes' component={FixedIncomes} />
        <Route exact path='/ProfitGoals' component={ProfitGoals} />
        <Route exact path='/RecuringExpenses' component={RecuringExpenses} />
        <Route exact path='/RecuringIncomes' component={RecuringIncomes} />
        <Route exact path='/reports' component={Reports} />
        <Route exact path='/Admin' component={Admin} />
        <Route exact path='/Income' component={Income} />
        <Route exact path='/Expence' component={Expence} />
        <Route exact path='/Category' component={Category} />
     
        {/* <Route exact path='/addcategory' component={addcategory} /> */}

        
      </Switch>

</Router>
    
        </div>

  );
}

export default App;
